
function handleClick(event) {
    let col = event.currentTarget;
    let boxInfo = getNextBox(col);

    if (boxInfo != null) {
        doMove(boxInfo.box);

        if (checkVertical(col) == currentPlayer) {
            alert("vertical win");
            for (c of columns) {
                c.removeEventListener('click', handleClick);
            }
        }

        if (checkHorizontal(boxInfo.row) == currentPlayer) {
            alert('horizontal win');
            for (c of columns) {
                c.removeEventListener('click', handleClick);
            }
        }
        togglePlayer();
    }
}



function checkHorizontal(row) {
    const cols = document.getElementsByClassName('column');
    let count = 1;
    let who = 'none';

    for (let i = 0; i < cols.length; i++) {
        const boxes = cols[i].getElementsByClassName('box');
        const b = boxes[row].dataset.selected;
        if (who == b) {
            count++
            if (count == 4) {
                return who
            }
        } else {
            count = 1;
            who = b;
        }
    }

    return 'none';
}

function checkVertical(col) {
    const boxes = col.getElementsByClassName('box');
    let count = 1;
    let who = 'none';

     for (let i = boxes.length; i > 0; i--) {
        let b = boxes[i -1].dataset.selected;
        console.log (b)
        if (who == b){
            count++;
            if (count == 4) {
                return who
            }
        } else {
            count = 1;
            who = b;
        }
    }
    return 'none';
}

function getNextBox(col) {
    const boxes = col.getElementsByClassName('box');
    let i = 0;
    for (b of boxes) {
        if (b.getAttribute('data-selected') == 'none') {
            return {box: b, row: i};
        }
        i++;
    }
    return null;
}

function doMove(box) {
    drawCircle(box);
    box.setAttribute('data-selected', currentPlayer);
}

function togglePlayer() {
    if (currentPlayer == player1) {
        currentPlayer = player2;
    } else {
        currentPlayer = player1;
    }
}

function drawCircle(box) {
    let circleDiv = document.createElement('div');
    circleDiv.className = "circle";
    circleDiv.style.backgroundColor = currentPlayer;
    box.appendChild(circleDiv);
}


const player1 = 'black';
const player2 = 'red';
let currentPlayer = player1;

let columns = document.getElementsByClassName('column');
for (c of columns) {
    c.addEventListener('click', handleClick);
}
